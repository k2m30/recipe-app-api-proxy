FROM nginxinc/nginx-unprivileged:1-alpine

COPY ./default.conf.tpl /etc/nginx/
COPY ./uwsgi_params /etc/nginx/

ENV APP_HOST=app
ENV APP_PORT=9000
ENV LISTEN_PORT=8000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh

USER nginx
CMD ["/entrypoint.sh"]